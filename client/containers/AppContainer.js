import React, { Component } from 'react';
import { connect } from "react-redux";
// reducer actions
import { registerApplication } from 'Reducers/common/actions';
import { getMemberStatus } from 'Reducers/member/actions/getStatus';

import { loaderOn, loaderOff } from 'Helpers/Loaders';

class AppContainer extends Component
{
    constructor(props){
        super(props);

        this.state = {
            loading: true
        }
    }
    componentWillMount() {
        this.props.registerApplication();
        this.props.getMemberStatus();
    }

    componentDidMount() {
        loaderOn();
    }

    componentWillReceiveProps(nextProps) {
        if(this.props.isRequesting !== nextProps.isRequesting)
        {
            console.log('finish loading');
            this.setState({
                loading: false
            });
            loaderOff();
        }
    }

    render() {
        const { loading } = this.state;
        return (
            <React.Fragment>
                {!loading ? this.props.children : null}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    const commonJS = state.get('common').toJS();
    const memberJS = state.get('member').toJS();
    const { application: { appInfo } } = commonJS;
    const { auth: { isLoggedIn }, status: { isRequesting } } = memberJS;
    return {
        appInfo,
        isRequesting,
        isLoggedIn,
    };
};

const mapDispatchToProps = {
    registerApplication,
    getMemberStatus
};

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);