import { Component } from 'react';
import { connect } from "react-redux";
import { reduxForm, SubmissionError } from 'redux-form/immutable';
// import PhoneInput from 'react-phone-number-input'
import { Button, FormField } from 'Components/form';
import { ErrorFont } from 'Components/common/Error';

import { memberLogin } from 'Client/reducers/member/actions/auth';

class LoginForm extends Component
{
    constructor(props) {
        super(props);

        this.validateSubmit = this.validateSubmit.bind(this)
    }

    validateSubmit = values => {
        let errorObj = null;
        const english = /^[a-zA-Z]+$/;

        if (!values.get('phone')) {
            errorObj = {
                ...errorObj,
                ...{
                    phone: 'Please fill in a correct phone number',
                },
            };
        }
        
        if (!values.get('password')) {
            errorObj = {
                ...errorObj,
                ...{
                    password: 'Please fill in password',
                },
            };
        }

        if (errorObj) {
            throw new SubmissionError(errorObj);
        }

        const args = {
            phone: values.get('phone'),
            password: values.get('password'),
            country: '5c7ce286d080dd49e0a8f02c'
        }

        
        return this.props.memberLogin(args);
    }

    render() {
        const { handleSubmit, customError } = this.props;

        return (
            <form id="form-login" className="loginForm" onSubmit={handleSubmit(this.validateSubmit)}>
                <FormField
                    type="tel"
                    name="phone"
                    placeholder="Phone number"
                    padding="10px 20px 10px 83px"
                >
                    <span className="postal">+852</span>
                </FormField>
                <FormField
                    type="password"
                    name="password"
                    placeholder="Password"
                    />
                <FormField
                    type="checkbox"
                    name="rememberMe"
                    placeholder="Remember me"
                    />
                    <ErrorFont
                        bottom='-48px'
                        margin='0'
                        >{customError ? customError : null}</ErrorFont>
                <div className="text-center mt-4">
                    <Button bgColor="yellow" width="100%">Continue</Button>
                </div>
            </form>
        )
    }
}

const mapStateToProps = state => {
    const memberJS = state.get('member').toJS();
    const { error } = memberJS.auth;
    return ({
        customError: error
    });
}

const mapDispatchToProps = {
    memberLogin
}

const reduxLoginForm = reduxForm({
    form: 'form-login',
})(LoginForm)

export default connect(mapStateToProps, mapDispatchToProps)(reduxLoginForm);
