import React, { Component } from 'react';
import { connect } from "react-redux";
import { withRouter } from 'next/router';

class PrivateRoute extends Component {

    constructor(props) {
        super(props);
        const { isLoggedIn } = props;

        if(!isLoggedIn) {
            const { router } = props;
            router.push('/signin');
        }
    }

    render() {
        return (
            <React.Fragment>
                {this.props.children}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    const memberJS = state.get('member').toJS();
    const { auth: { isLoggedIn }, status: { isRequesting } } = memberJS;
    return ({
        isRequesting,
        isLoggedIn,
    });
}

const mapDispatchToProps = {

}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(PrivateRoute));