import { Component } from 'react';
import Link from 'Components/common/Link';
import SubscriptionForm from 'Client/containers/subscription/SubscriptionForm';
import css from 'Styles/footer/footerContainer.scss';

class FooterContainer extends Component
{
    render() {
        return (
            <footer>
                <div className="container">
                    <div className="clearfix">
                        <div className={`${css.footerSubscription} f-left`}>
                            <SubscriptionForm/>
                        </div>
                        <div className={`${css.footerIcon} f-right`}>
                            <Link href="#"><i className="fas fa-envelope"></i></Link>
                            <Link href="#"><i className="fab fa-facebook-f"></i></Link>
                            <Link href="#"><i className="fab fa-instagram"></i></Link>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
}

export default FooterContainer;