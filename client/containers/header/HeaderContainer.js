import React, { Component } from "react";
import { connect } from 'react-redux';
import Link from 'Components/common/Link';
import TopLink from 'Components/header/TopLink';
import Navigation from 'Components/header/Navigation';
import SearchForm from 'Client/containers/search/SearchForm';
import css from 'Styles/header/headerContainer.scss';

class HeaderContainer extends Component {

    componentWillReceiveProps(nextProps) {
        console.log('can i get the props? ', nextProps);
    }

    render() {
        const { isLoggedIn } = this.props;
        console.log('isloggedin?', this.props);
        return (
            <header>
                <div className={css.headerTop}>
                    <div className="container">
                        <div className="clearfix">
                            <TopLink
                                isLoggedIn={isLoggedIn}
                            />
                        </div>
                    </div>
                </div>
                <div className={css.headerMain}>
                    <div className="container">
                        <div className="clearfix">
                            <div className="f-left">
                                <Link className={css.logo} href="/"><img src="/static/images/logo.png"/></Link>
                            </div>
                            <div className={`${css.headerMainRightWrapper} f-right`}>
                                <SearchForm/>
                                <Link href="/checkout"><i className="fas fa-shopping-cart"></i></Link>
                                <Link href="/"><i className="far fa-heart"></i></Link>
                                <Link href="">Advanced Search</Link>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}

const mapStateToProps = state => {
    const memberJS = state.get('member').toJS();
    const { auth: { isLoggedIn } } = memberJS;
    return {
        isLoggedIn
    };
}

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderContainer);