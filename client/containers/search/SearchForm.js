import { Component } from "react";
import { reduxForm, SubmissionError } from 'redux-form/immutable';
import { Button, FormField } from 'Components/form';
import css from 'Styles/search/searchForm.scss';

class SearchForm extends Component
{
    constructor(props) {
        super(props);

        this.validateSubmit = this.validateSubmit.bind(this)
    }

    validateSubmit(values)
    {

    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <form id="form-search" className={css.formSearch} noValidate onSubmit={handleSubmit(this.validateSubmit)}>
                <FormField
                    type='text'
                    name='search'
                    placeholder='Search rewards'
                    margin="0"
                >
                    <Button width="auto" padding="5px 10px"><i className="fas fa-search"></i></Button>
                </FormField>
            </form>
        )
    }
}

export default reduxForm({
    form: 'form-search',
    persistentSubmitErrors: true,
})(SearchForm);
