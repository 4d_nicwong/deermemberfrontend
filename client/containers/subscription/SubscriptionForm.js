import { Component } from 'react';
import { reduxForm, SubmissionError } from 'redux-form/immutable';
import { Button, FormField } from 'Components/form';
import css from 'Styles/subscription/subscriptionForm.scss';
class SubscriptionForm extends Component
{
    constructor(props) {
        super(props);

        this.validateSubmit = this.validateSubmit.bind(this)
    }

    validateSubmit(values)
    {

    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <form id="form-subscribe" className={`${css.formSubscribe}`} noValidate onSubmit={handleSubmit(this.validateSubmit)}>
                <FormField
                    type='email'
                    name='email'
                    placeholder='Email address'
                    margin="0"
                >
                    <Button>Subscribe</Button>
                </FormField>
            </form>
        )
    }
}
export default reduxForm({
    form: 'form-subscribe',
    persistentSubmitErrors: true,
})(SubscriptionForm);
