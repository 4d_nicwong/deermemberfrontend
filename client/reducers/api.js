const API_ENDPOINT_ADDRESS = "http://deer.4d.com.hk/api/v1";

export const api_urls = {
    getStatus: `${API_ENDPOINT_ADDRESS}/member/status`,
    login_url: `${API_ENDPOINT_ADDRESS}/user/signin`,
    getCategory: `${API_ENDPOINT_ADDRESS}/category`,
    getProductList: `${API_ENDPOINT_ADDRESS}/category/`,
    getProductDetail: `${API_ENDPOINT_ADDRESS}/product/`,
    redeemProduct: `${API_ENDPOINT_ADDRESS}/member/{{prizeID}}/redeem`,
}