import Immutable from 'immutable';
import { handleActions } from 'redux-actions';
import { api_urls } from 'Reducers/api';
import request from 'Client/utils/request';



export const SET_PRIZE_QUANTITY = 'SET_PRIZE_QUANTITY';

export const FETCH_REDEEM_PRIZE_LOADING = 'FETCH_REDEEM_PRIZE_LOADING';
export const FETCH_REDEEM_PRIZE_SUCCESS = 'FETCH_REDEEM_PRIZE_SUCCESS';
export const FETCH_REDEEM_PRIZE_FAIL = 'FETCH_REDEEM_PRIZE_FAIL';

// store action
export const setQuantity = quantity => ({
    type: SET_PRIZE_QUANTITY,
    quantity,
});

// redeem prize
export const fetchRedeemPrizeLoading = () => ({
    type: FETCH_REDEEM_PRIZE_LOADING,
});
export const fetchRedeemPrizeSuccess = () => ({
    type: FETCH_REDEEM_PRIZE_SUCCESS,
});
export const fetchRedeemPrizeFail = error => ({
    type: FETCH_REDEEM_PRIZE_FAIL,
    error,
});


// dispatch function
export const setPrizeQuantity = quantity => (dispatch, getState) => {
    dispatch(setQuantity(quantity));
};

export const fetchRedeemPrize = data => (dispatch, getState) => {
    dispatch(fetchRedeemPrizeLoading());
    try {
        const token = localStorage.getItem('deerMemberToken');
        request.post(
            {
                url: api_urls.redeemProduct.replace('{{prizeID}}', data.prizeID),
                headers: {
                    Authorization: 'Bearer ' + token,
                },
                data: {
                    quantity: data.quantity
                }
            }
        )
        .then(
            response => {
                if(!response.status) {
                    return dispatch(fetchRedeemPrizeFail(response.errorMessage))
                }
                dispatch(fetchRedeemPrizeSuccess(response.data.data));
            }
        )
        .catch(error => {
            console.log('#### ERROR #####', error);
            dispatch(fetchRedeemPrizeFail(error));
        });
    }
    catch (error) {
        dispatch(fetchRedeemPrizeFail(error));
        return {};
    }
};

const checkoutState = Immutable.fromJS({
    quantity: 1,
    redeemPrizeLoading: false,
    redeemPrizeSuccess: false,
    redeemPrizeFail: false,
});

const checkoutReducer = {
    SET_PRIZE_QUANTITY: (state, action) => (
        state
            .setIn(['quantity'], action.quantity)
    ),
    FETCH_REDEEM_PRIZE_LOADING: state => (
        state
            .setIn(['redeemPrizeLoading'], true)
    ),
    FETCH_REDEEM_PRIZE_SUCCESS: state => (
        state
            .setIn(['redeemPrizeLoading'], false)
            .setIn(['redeemPrizeSuccess'], true)
    ),
    FETCH_REDEEM_PRIZE_FAIL: (state, action) => (
        state
            .setIn(['redeemPrizeLoading'], false)
            .setIn(['redeemPrizeFail'], action.error)
    ),
};
  
const checkoutReducers = handleActions({
    ...checkoutReducer,
}, checkoutState);

export default checkoutReducers;
  