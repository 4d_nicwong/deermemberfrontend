import { handleActions } from 'redux-actions';
import CommonState from '..';

import {
  COMMON_REGISTER_APP,
  COMMON_REGISTER_APP_SUCCESS,
  COMMON_REGISTER_APP_FAIL,
} from '../types';

const applicationReducer = {
  COMMON_REGISTER_APP: state => (
    state
      .setIn(['application', 'isLoading'], false)
      .setIn(['application', 'isSuccess'], false)
      .setIn(['application', 'appInfo'], null)
  ),
  COMMON_REGISTER_APP_SUCCESS: (state, payload) => (
    state
      .setIn(['application', 'isLoading'], true)
      .setIn(['application', 'isSuccess'], true)
      .setIn(['application', 'appInfo'], payload)
  ),
  COMMON_REGISTER_APP_FAIL: state => (
    state
      .setIn(['application', 'isLoading'], true)
      .setIn(['application', 'isSuccess'], false)
  ),
};

const commonReducers = handleActions({
	...applicationReducer,
}, CommonState);

export default commonReducers;
