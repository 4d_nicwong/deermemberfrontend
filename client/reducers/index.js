import { combineReducers } from 'redux-immutable';
import { reducer as formReducer } from 'redux-form/immutable';
import commonReducers from './common/reducers';
import memberReducers from './member/reducers';
import checkoutReducer from './checkout';
import prizeReducer from './prize';

const rootReducer = combineReducers({
	form: formReducer,
	checkout: checkoutReducer,
	prize: prizeReducer,
	...memberReducers,
	...commonReducers,
});

export default rootReducer;
