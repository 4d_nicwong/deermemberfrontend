import request from 'Client/utils/request';
import { api_urls } from 'Reducers/api';
// Action Types
import {
    IS_LOGGED_IN,
    MEMBER_REQUEST_LOGIN,
    MEMBER_REQUEST_LOGIN_SUCCESS,
    MEMBER_REQUEST_LOGIN_FAIL
} from '../types';

export const settingLoggedIn = () => ({
    type: IS_LOGGED_IN,
});
export const menberLoginFetch = () => ({
  	type: MEMBER_REQUEST_LOGIN,
});
export const memberLoginSuccess = () => ({
  	type: MEMBER_REQUEST_LOGIN_SUCCESS,
});
export const memberLoginFail = message => ({
  	type: MEMBER_REQUEST_LOGIN_FAIL,
 	message,
});

// to be fixed as a static app info endpoint to register the application
// const constantAppInfoUrl = 'http://localhost:3013/api/appInfo';

export const setIsLoggedIn = () => (dispatch, getState) => {
    dispatch(settingLoggedIn());
}

export const memberLogin = data => (dispatch, getState) => {
    try{
        request.post(
            {
                url: api_urls.login_url,
                data: data,
            }
        )
        .then(
            (response) => {
                if(!response.status) {
                    return dispatch(memberLoginFail(response.errorMessage))
                }
                localStorage.setItem('deerMemberToken', response.data.data.loginToken);
                dispatch(memberLoginSuccess());
            },
        ).catch((error) => {
            dispatch(memberLoginFail('The username or password is incorrect.'));
        });
    }
    catch (e) {
        dispatch(memberLoginFail(e.code));

        return {};
    }
};
