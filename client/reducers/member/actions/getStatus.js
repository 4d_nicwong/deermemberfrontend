import request from 'Client/utils/request';
import { api_urls } from 'Reducers/api';
import { setIsLoggedIn } from 'Reducers/member/actions/auth';

import {
    MEMBER_REQUEST_STATUS,
    MEMBER_REQUEST_STATUS_SUCCESS,
    MEMBER_REQUEST_STATUS_FAIL,
} from '../types';

export const memberRequestStatus = () => ({
    type: MEMBER_REQUEST_STATUS,
});
export const memberRequestStatusSuccess = data => ({
    type: MEMBER_REQUEST_STATUS_SUCCESS,
    data
});
export const memberRequestStatusFail = message => ({
    type: MEMBER_REQUEST_STATUS_FAIL,
    message,
});

export const getMemberStatus = data => (dispatch, getState) => {
    dispatch(memberRequestStatus());
    try{
        const token = localStorage.getItem('deerMemberToken');
        request.get(
            {
                url: api_urls.getStatus,
                data: data,
                headers: {
                    Authorization: 'Bearer ' + token,
                }
            }
        )
        .then(
            (response) => {
                if(!response.status) {
                    return dispatch(memberRequestStatusFail(response.errorMessage))
                }
                dispatch(setIsLoggedIn());
            },
        ).catch((error) => {
            dispatch(memberRequestStatusFail('You must signin your member account first.'));
        });
    }
    catch (e) {
        console.log('e>>>>>>>>>>', e);
        dispatch(memberRequestStatusFail(e.code));

        return {};
    }
};

export const fetchMemberStatus = data => (dispatch, getState) => {
    dispatch(memberRequestStatus());
    try{
        const token = localStorage.getItem('deerMemberToken');
        request.get(
            {
                url: api_urls.getStatus,
                data: data,
                headers: {
                    Authorization: 'Bearer ' + token,
                }
            }
        )
        .then(
            (response) => {
                if(!response.status) {
                    return dispatch(memberRequestStatusFail(response.errorMessage))
                }
                dispatch(memberRequestStatusSuccess(response.data.data));
            },
        ).catch((error) => {
            dispatch(memberRequestStatusFail('You must signin your member account first.'));
        });
    }
    catch (e) {
        console.log('e>>>>>>>>>>', e);
        dispatch(memberRequestStatusFail(e.code));

        return {};
    }
};