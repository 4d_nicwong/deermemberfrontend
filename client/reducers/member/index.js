import Immutable from 'immutable';

const MemberState = Immutable.fromJS({
	status: {
		info: null,
		isRequesting: false,
		isSuccess: false,
		error: null
	},
	auth: {
		isLoggedIn: false,
	    isLoading: false,
        isSuccess: false,
        error: null
	},
});

export default MemberState;
