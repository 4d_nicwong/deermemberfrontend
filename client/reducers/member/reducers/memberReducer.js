import { handleActions } from 'redux-actions';
import MemberState from '..';

import {
    IS_LOGGED_IN,
    MEMBER_REQUEST_LOGIN,
    MEMBER_REQUEST_LOGIN_SUCCESS,
    MEMBER_REQUEST_LOGIN_FAIL,
    MEMBER_REQUEST_STATUS,
    MEMBER_REQUEST_STATUS_SUCCESS,
    MEMBER_REQUEST_STATUS_FAIL
} from '../types';

const memberReducer = {
    IS_LOGGED_IN: state => (
        state
        .setIn(['auth', 'isLoggedIn'], true)
    ),
    MEMBER_REQUEST_LOGIN: state => (
        state
        .setIn(['auth', 'isLoggedIn'], false)
        .setIn(['auth', 'isLoading'], true)
        .setIn(['auth', 'isSuccess'], false)
        .setIn(['auth', 'error'], null)
    ),
    MEMBER_REQUEST_LOGIN_SUCCESS: (state, payload) => (
        state
        .setIn(['auth', 'isLoggedIn'], true)
        .setIn(['auth', 'isLoading'], false)
        .setIn(['auth', 'isSuccess'], true)
        .setIn(['auth', 'error'], null)
    ),
    MEMBER_REQUEST_LOGIN_FAIL: (state, payload) => (
        state
        .setIn(['auth', 'isLoggedIn'], false)
        .setIn(['auth', 'isLoading'], false)
        .setIn(['auth', 'isSuccess'], false)
        .setIn(['auth', 'error'], payload.message)
    ),
    MEMBER_REQUEST_STATUS: state => (
        state
        .setIn(['status', 'isRequesting'], true)
        .setIn(['status', 'isSuccess'], false)
        .setIn(['status', 'error'], null)
    ),
    MEMBER_REQUEST_STATUS_SUCCESS: (state, payload) => (
        state
        .setIn(['status', 'info'], payload.data)
        .setIn(['status', 'isRequesting'], false)
        .setIn(['status', 'isSuccess'], true)
        .setIn(['status', 'error'], null)
    ),
    MEMBER_REQUEST_STATUS_FAIL: (state, payload) => (
        state
        .setIn(['status', 'isRequesting'], false)
        .setIn(['status', 'isSuccess'], false)
        .setIn(['status', 'error'], payload.message)
    )
};

const memberReducers = handleActions({
	...memberReducer,
}, MemberState);

export default memberReducers;
