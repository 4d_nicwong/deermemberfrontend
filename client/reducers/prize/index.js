import Immutable from 'immutable';
import { handleActions } from 'redux-actions';
import { api_urls } from 'Reducers/api';
import request from 'Client/utils/request';



/* action name */
export const FETCH_PRIZE_LIST_LOADING = 'FETCH_PRIZE_LIST_LOADING';
export const FETCH_PRIZE_LIST_SUCCESS = 'FETCH_PRIZE_LIST_SUCCESS';
export const FETCH_PRIZE_LIST_FAIL = 'FETCH_PRIZE_LIST_FAIL';

export const FETCH_PRIZE_DETAIL_LOADING = 'FETCH_PRIZE_DETAIL_LOADING';
export const FETCH_PRIZE_DETAIL_SUCCESS = 'FETCH_PRIZE_DETAIL_SUCCESS';
export const FETCH_PRIZE_DETAIL_FAIL = 'FETCH_PRIZE_DETAIL_FAIL';



/* store action */
// fetch prize list
export const fetchPrizeListLoading = () => ({
    type: FETCH_PRIZE_LIST_LOADING,
});
export const fetchPrizeListSuccess = data => ({
    type: FETCH_PRIZE_LIST_SUCCESS,
    data,
});
export const fetchPrizeListFail = error => ({
    type: FETCH_PRIZE_LIST_FAIL,
    error,
});

// fetch prize detail
export const fetchPrizeDetailLoading = () => ({
    type: FETCH_PRIZE_DETAIL_LOADING,
});
export const fetchPrizeDetailSuccess = data => ({
    type: FETCH_PRIZE_DETAIL_SUCCESS,
    data,
});
export const fetchPrizeDetailFail = error => ({
    type: FETCH_PRIZE_DETAIL_FAIL,
    error,
});



/* dispatch function */
export const fetchPrizeList = data => (dispatch, getState) => {
    dispatch(fetchPrizeListLoading());
    try {
        // const token = localStorage.getItem('deerMemberToken');
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzdjZjM3MjJiMDAxZTc2NzIyYzI2ZjUiLCJmdWxsbmFtZSI6eyJsYXN0IjoiY2hhbiIsImZpcnN0IjoidGVycnkifSwiZW1haWwiOiJ0ZXJyeWNoYW5ANGQuY29tLmhrIiwicGhvbmUiOjEyMzQ1Njc4LCJsYW5ndWFnZSI6ImVuIiwibm90aWZpY2F0aW9uIjpmYWxzZSwidGl0bGUiOiJDRU8iLCJpYXQiOjE1NTE3NTk3MjgsImV4cCI6MTU1MTc2MzMyOH0.ARRslUEJGfre10mf59Zuj4JFeBDSMseew2vONJ_vDzI';
        const categoryID = '5c7d03500109730df0df0fcb';
        request.get(
            {
                url: api_urls.getProductList + categoryID,
                headers: {
                    Authorization: 'Bearer ' + token,
                }
            }
        )
        .then(
            response => {
                if(!response.status) {
                    return dispatch(fetchPrizeListFail(response.errorMessage))
                }
                dispatch(fetchPrizeListSuccess(response.data.data));
            }
        )
        .catch(error => {
            dispatch(fetchPrizeListFail('[request catch] Fetch prize list fail.'));
        });
    }
    catch (error) {
        dispatch(fetchPrizeListFail(error));
        return {};
    }
};

export const fetchPrizeDetail = id => (dispatch, getState) => {
    dispatch(fetchPrizeDetailLoading());
    try {
        // const token = localStorage.getItem('deerMemberToken');
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzdjZjM3MjJiMDAxZTc2NzIyYzI2ZjUiLCJmdWxsbmFtZSI6eyJsYXN0IjoiY2hhbiIsImZpcnN0IjoidGVycnkifSwiZW1haWwiOiJ0ZXJyeWNoYW5ANGQuY29tLmhrIiwicGhvbmUiOjEyMzQ1Njc4LCJsYW5ndWFnZSI6ImVuIiwibm90aWZpY2F0aW9uIjpmYWxzZSwidGl0bGUiOiJDRU8iLCJpYXQiOjE1NTE3NTk3MjgsImV4cCI6MTU1MTc2MzMyOH0.ARRslUEJGfre10mf59Zuj4JFeBDSMseew2vONJ_vDzI';
        request.get(
            {
                url: api_urls.getProductDetail + id,
                headers: {
                    Authorization: 'Bearer ' + token,
                }
            }
        )
        .then(
            response => {
                if(!response.status) {
                    return dispatch(fetchPrizeDetailFail(response.errorMessage))
                }
                dispatch(fetchPrizeDetailSuccess(response.data.data));
            }
        )
        .catch(error => {
            dispatch(fetchPrizeDetailFail('[request catch] Fetch prize detail fail.'));
        });
    }
    catch (error) {
        dispatch(fetchPrizeDetailFail(error));
        return {};
    }
};



const prizeState = Immutable.fromJS({
    prizeList: null,
    prizeListLoading: false,
    prizeListSuccess: false,
    prizeListFail: false,

    prizeDetail: null,
    prizeDetailLoading: false,
    prizeDetailSuccess: false,
    prizeDetailFail: false,
});

const prizeReducer = {
    FETCH_PRIZE_LIST_LOADING: state => (
        state
            .setIn(['prizeListLoading'], true)
    ),
    FETCH_PRIZE_LIST_SUCCESS: (state, action) => (
        state
            .setIn(['prizeListLoading'], false)
            .setIn(['prizeListSuccess'], true)
            .setIn(['prizeList'], action.data)
    ),
    FETCH_PRIZE_LIST_FAIL: (state, action) => (
        state
            .setIn(['prizeListLoading'], false)
            .setIn(['prizeListFail'], true)
    ),
    // ----------------------------------------
    FETCH_PRIZE_DETAIL_LOADING: state => (
        state
            .setIn(['prizeDetailLoading'], true)
    ),
    FETCH_PRIZE_DETAIL_SUCCESS: (state, action) => (
        state
            .setIn(['prizeDetailLoading'], false)
            .setIn(['prizeDetailSuccess'], true)
            .setIn(['prizeDetail'], action.data)
    ),
    FETCH_PRIZE_DETAIL_FAIL: (state, action) => (
        state
            .setIn(['prizeDetailLoading'], false)
            .setIn(['prizeDetailFail'], true)
    ),
};
  
const prizeReducers = handleActions({
    ...prizeReducer,
}, prizeState);

export default prizeReducers;
  