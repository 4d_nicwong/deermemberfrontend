import React, { Component } from "react";
import { connect } from "react-redux";
import Link from 'Components/common/Link';

import { fetchPrizeList } from 'Reducers/prize';
import Styles from 'Styles/single/prizeList.scss';


class PrizeList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            prizeList: [],
            totalPrize: 0,
        }
    }

    componentWillMount() {
        this.props.fetchPrizeList();
    }

    componentWillReceiveProps(nextProps, nextState) {
        const { prizeListSuccess, prizeList } = nextProps;
        if (prizeListSuccess) {
            this.setState({
                prizeList: prizeList.product,
                totalPrize: prizeList.total,
            })
        }
    }

    renderHeader() {
        const { totalPrize } = this.state;
        return (
            <div className='pricelistHeader'>
                <div>Items 1-10 of {totalPrize}</div>
                <div>
                    <span>Sort By</span>
                    <div>
                        <select>
                            <option>Recommend</option>
                            <option>Point</option>
                        </select>
                    </div>
                    <i className="fas fa-angle-double-up"/>
                </div>
            </div>
        )
    }

    renderList() {
        const { prizeList } = this.state;
        const listItems = prizeList.map((item, key) => {
            return (
                <div className='row listItem' key={key}>
                    <div className='col-5 imageCol'><img src={item.picture.url}/></div>
                    <div className='col-7 infoCol'>
                        <div><h4>{item.name}</h4></div>
                        <div><i className="fas fa-copyright"></i><span>{item.pointNeeded}</span></div>
                        <div><Link route="/prize-detail" params={{id: item._id}}>Read more</Link></div>
                        <div>
                            <i className="fab fa-facebook"/>
                            <i className="far fa-envelope"/>
                            <i className="far fa-copy"/>
                        </div>
                    </div>
                </div>
            )
        });
        return (
            <div className='list'>
                {listItems}
            </div>
        )
    }

    renderFooter() {
        return (
            <div className="pricelistFooter">
                <div>
                    <span className="activePage">1</span>
                    <span>2</span>
                    <span>3</span>
                    <i className="fas fa-angle-right"/>
                </div>
                <div>
                    <span>Show</span>
                    <div>
                        <select>
                            <option>10</option>
                            <option>20</option>
                            <option>30</option>
                        </select>
                    </div>
                    <span>Items per page</span>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div className="prizeList">
                {this.renderHeader()}
                {this.renderList()}
                {this.renderFooter()}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    prizeListSuccess: state.get('prize').toJS().prizeListSuccess,
    prizeList: state.get('prize').toJS().prizeList,
});

const mapDispatchToProps = {
    fetchPrizeList,
};

const connectPrizeList = connect(mapStateToProps, mapDispatchToProps)(PrizeList);

export default connectPrizeList;