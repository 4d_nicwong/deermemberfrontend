import React, { Component } from "react";
import Link from 'Components/common/Link';
import Styles from 'Styles/common/breadcrumb.scss';

class Breadcrumb extends Component{

    constructor(props) {
        super(props);
    }

    render() {
        const { links } = this.props;
        const breadcrumbItem = links.map((item, key) => {
            return key === links.length - 1
            ? <span key={key} className={Styles.active}>{item.text}</span>
            : <span key={key}><Link href={item.href}>{item.text}</Link></span>;
        })
        return (
            <div className={Styles.breadcrumb}>
                {breadcrumbItem}
            </div>
        )
    }
}

export default Breadcrumb;