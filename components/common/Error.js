import React from 'react';
import styled from 'styled-components';

const ErrorFont = styled.div`
    color: #fd3a3a;
    font-weight: bold;
    height: 1.5em;
    ${props => props.position ? `position: ${props.position};`: ``}
    ${props => props.left ? `left: ${props.left};`: ``}
    ${props => props.right ? `right: ${props.right};`: ``}
    ${props => props.top ? `top: ${props.top};`: ``}
    ${props => props.bottom ? `bottom: ${props.bottom};`: ``}
    ${props => props.margin ? `margin: ${props.margin};`: `margin: 0 0 35px 0;`}
    ${props => props.textAlign ? `text-align: ${props.textAlign};`: ``}
    ${props => {
        if (props.textAlign === 'center' && props.position === 'absolute')
            return 'width: 100%;'
    }}
`;

export { ErrorFont };
