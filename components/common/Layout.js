import { Component } from 'react';
import { Container } from 'react-bootstrap';
import HeaderContainer from 'Client/containers/header/HeaderContainer';
import FooterContainer from 'Client/containers/footer/FooterContainer';

class Layout extends Component
{
    render() {
        return (
        	<div id="web-container">
                <HeaderContainer/>
                <main>
                    <Container>
                        {this.props.children}
                    </Container>
                </main>
                <FooterContainer/>
            </div>
        )
    }
}

export default Layout;