// style sheets
/*
** Maintain the SPA approach
** Each of hyperlink must use this higher level component
** Terry Chan
** 22/10/2018
*/

import React from 'react'
import { withRouter } from 'next/router';

class ActiveLink extends React.Component {

  // componentDidMount() {
  //   const { router, href } = this.props;
  //   router.prefetch(href)
  // }

  render() {
    const { children, router, href, className, params = {} } = this.props
    return (
        <a 
          className={className} 
          onClick={ () => {
              router.push(href, params)
            }
          }
          tabIndex="0"
        >
          {children}
        </a>
    )
  }

}

export default withRouter(ActiveLink)