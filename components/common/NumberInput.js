import React, { Component, PureComponent } from "react";
import Styles from 'Styles/common/numberInput.scss';

class NumberInput extends PureComponent {

    constructor(props) {
        super(props);
        this.number = props.value || props.min || 1;
        this.reduceNumber = this.reduceNumber.bind(this);
        this.increaseNumber = this.increaseNumber.bind(this);
    }

    reduceNumber() {
        const numberInput = this.refs.numberInput;
        numberInput.focus();
        numberInput.stepDown(1);
        if (this.props.getNumber) {
            this.props.getNumber(parseInt(numberInput.value));
        }
    }

    increaseNumber() {
        const numberInput = this.refs.numberInput;
        numberInput.focus();
        numberInput.stepUp(1);
        if (this.props.getNumber) {
            this.props.getNumber(parseInt(numberInput.value));
        }
    }

    render() {
        const { min = 1, max = 100 } = this.props;
        return (
            <span className={Styles.numberInput}>
                <a><i className="fas fa-minus" onClick={this.reduceNumber}></i></a>
                <input type='number' min={min} max={max} value={this.number} readOnly ref='numberInput'/>
                <a><i className="fas fa-plus" onClick={this.increaseNumber}></i></a>
            </span>
        )
    }
}

export default NumberInput;