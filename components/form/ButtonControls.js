import styled, { css } from 'styled-components';

const buttonType = props => props.type ? props.type : 'submit';
const Button = styled.button.attrs({
    type: buttonType
})`
    outline: none;
    background: none;
    padding: 5px 20px;
    width: ${props => props.width ? props.width : ` 100% `};
    padding: ${props => props.padding ? props.padding : ` 10px 20px `};
    background-color: ${props => props.bgColor ? props.bgColor : `none `};
    border: ${props => props.border ? props.border : ` 0 `};
    &:focus { outline: none; }
    ${props => {
        const { bgColor } = props;
        const colorTone = {
            yellow: {
                background: '#F4D43E',
                color: '#382520',
                backgroundHover: '#382520',
                colorHover: '#F4D43E',
            },
        }
        if (bgColor && colorTone[bgColor]) {
            return `
                background: ${colorTone[bgColor].background};
                color: ${colorTone[bgColor].color}; 
                border: ${colorTone[bgColor].background};
                -webkit-transition: background 0.2s linear;
                transition: background 0.2s linear;
                &:hover{
                    color: ${colorTone[bgColor].colorHover};
                    background: ${colorTone[bgColor].backgroundHover}; 
                    border: ${colorTone[bgColor].borderHover};
                }
            `;
        }
    }}
`

export { Button };