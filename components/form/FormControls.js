import React from 'react';
import { Field } from 'redux-form/immutable';
import styled, { css } from 'styled-components';
import { 
    Input,
    Checkbox,
    CheckboxInput
} from './FormStyles';
import { ErrorFont } from '../common/Error';

const Holder = styled.div`
    position: relative;
`;

const TelCodeHolder = styled.div`
    position: relative;
    display: inline-block;
    vertical-align: middle;
    select{
        position: absolute;
        left: 0;
        top: 0;
        bottom: 0;
        margin: auto;
        padding: 12px 5px;
        width: 100px;
    }
`;
const InputField = styled.div`
    position: relative;
    margin: ${props => props.hasError ? `0 0 35px 0;` : props.margin}
    ${props => 
        props.icon &&`
        i{
        display: block;
        position: absolute;
        top: 0;
        bottom: 0;
        margin: auto;
        font-size: 20px;
        max-width: 20px;
        max-height: 20px;
        width: 20px;
        height: 20px;
        text-align: center;
        ${props.borderless ? `
        left: 2px`
        :
        `left: 20px;`
        }
    }`
    }
`;

const CheckboxFormContainer = styled.div`
    position: relative;
    ${props => props.margin ? `margin: ${props.margin};` : null}
    input{
        opacity: 0;
        position: absolute;
        width: 21px;
        height: 21px;
        left: -3px;
        z-index:2;
    }
`;

class FormInput extends React.PureComponent{

    constructor(props){
        super(props);

        this.handleChecked = this.handleChecked.bind(this);
    }

    handleChecked(e, checked){
        const { input } = this.props;
        input.onChange(checked);
    }

    render(){
        const { input, type, placeholder, children, margin, label, icon, padding, customLength, error: customError, customErrorBottom, borderless, name, meta: { touched, error, warning }, ...props } = this.props;
        const customMargin = margin ? margin : '0 0 35px 0';
        switch (type) {
            case 'text':
            case 'password':
            case 'email':
            case 'number':
            case 'tel':
                return (
                    <InputField
                        icon={icon}
                        margin={customMargin}
                        borderless={borderless}
                    >
                        <Holder>
                            {label ? 
                            <label for={name}>{label}</label>
                            :
                            ''}
                            <Input
                                name={name}
                                type={type}
                                placeholder={placeholder}
                                icon={icon}
                                showError={!!error}
                                borderless={borderless}
                                maxLength={customLength}
                                padding={padding}
                                {...input}
                                {...props}
                            />
                            {children}
                        </Holder>
                        {customError || error ?
                        <ErrorFont
                            position='absolute'
                            left='0'
                            bottom={customErrorBottom ? customErrorBottom : '-26px'}
                            margin='0'
                        >{customError || error}</ErrorFont> : null}
                    </InputField>
                )
            case 'checkbox':
                return (
                    <CheckboxFormContainer margin="0 0 10px 0">
                        <input type={type} {...input} checked={input.checked}/>
                        <div>
                            <Checkbox checked={input.checked}>
                            </Checkbox>
                            <span>
                                {placeholder}
                            </span>
                        </div>
                    </CheckboxFormContainer>
                )
            case 'hidden':
                return (
                    <InputField>
                        <Input
                            name={name}
                            type={type}
                            {...input}
                            {...props}
                            />
                    </InputField>
                )
            default: {
                return (<input type={type} {...input} />);
            }
        }
    }
}

const renderField = props => <FormInput {...props} />;

export const FormField = props => <Field component={renderField} {...props} />;