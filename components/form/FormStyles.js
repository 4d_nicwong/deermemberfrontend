import styled from 'styled-components';

const BasicStyle = styled.div`
    width: 100%;
    background: #FFF;
    color: #58595B;
    outline: none;
    &::-webkit-input-placeholder{
        color: #9B9B9B
    }
    &::-moz-placeholder{
        color: #9B9B9B
    }
    &::-ms-input-placeholder{
        color: #9B9B9B
    }
`;

const BasicInput = BasicStyle.withComponent('input');
const Input = styled(BasicInput)`
    outline: none;
    font-size: 15px;
    max-width: 600px;
    background-color: #fff;
    border-radius: 3px;
    border: 1px solid transparent;
    box-shadow: 0 2px 4px 0 rgba(0,0,0,0.16),0 2px 10px 0 rgba(0,0,0,0.12);
    padding: ${props => props.padding ? props.padding : ` 10px 20px`};
`;

const Checkbox = styled.div`
    width: 18px;
    height: 18px;
    margin-right: 10px;
    border: 1px solid #58595B;
    border-radius: 5.6px;
    display: inline-block;
    vertical-align: middle;
    cursor: pointer;
    -webkit-transition: background 0.2s;
    transition: background 0.2s;
    background-color: #F8F8F8;
    background-size: 100%;
    background-repeat: no-repeat;
    ${props => props.checked &&
        `background-image: url('/static/images/icon/tick.svg')` 
    }
`;

export {
    Input,
    Checkbox,
}