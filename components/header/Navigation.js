import { Component } from 'react';
import Link from 'Components/common/Link';
import css from 'Styles/header/navigation.scss';

class Navigation extends Component
{

    renderNav()
    {
        const items = [
            { name: 'Our Recommendations', class: 'active' },
            { name: 'New' },
            { name: 'Club Travel' },
            { name: 'Electronics' },
            { name: 'Wine & Dine' },
            { name: 'Events' },
            { name: 'Family' },
            { name: 'Leisure & Entertainment' },
            { name: 'Pets' },
            { name: 'Charity' },
            { name: 'Fashion & Accessories' },
            { name: 'Beauty & Health' },
        ];
        return (
            items.map((item, i) => {
                return (
                    <li key={i} className={`${item.class && css.navChildActive}`}>
                        <Link prefetch href="/">{item.name}</Link>
                    </li>
                )
            })
        )
    }

    render()
    {
        return (
            <ul className={css.navigation}>
                {this.renderNav()}
            </ul>
        )
    }
}

export default Navigation;