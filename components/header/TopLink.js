import React, { Component } from "react";
import Link from 'Components/common/Link';
import css from 'Styles/header/topLink.scss';

class TopLink extends Component {

    renderLink() {
        const { isLoggedIn } = this.props;
        let items = [];
        if(isLoggedIn) {
            items = [
                {
                    name: 'My Account',
                    href: '/my-account'
                },
                {
                    name: 'My Order',
                    href: '/my-order'
                },
                {
                    name: <i className="fas fa-sign-out-alt"></i>,
                    href: '/logout'
                }
            ]
        }
        else{
            items = [
                {
                    name: 'Login',
                    href: '/signin'
                },
                {
                    name: 'Register',
                    href: '/'
                }
            ]
        }
        return (
            items.map((item, i) => {
                return (
                    <li key={i}>
                        <Link href={item.href}>{item.name}</Link>
                    </li>
                )
            })
        )
    }

    render() {
        const { isLoggedIn } = this.props;
        return (
            <ul className={`${css.topLink}`}>
                <li><Link href="/"><i className="fas fa-home"></i></Link></li>
                {this.renderLink()}
                <div>
                    <Link href="#">中</Link>
                </div>
            </ul>
        )
    }

}
export default TopLink;