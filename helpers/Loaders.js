export const loaderOn = () => {
    document.body.style.cursor = 'progress';
}

export const loaderOff = () => {
    document.body.style.cursor = 'default';
}