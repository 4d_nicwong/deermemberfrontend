const express 					= require('express');
const next 						= require('next');
const routes 					= require('./routes');
// store the base path
global.__base 					= __dirname;
// Configurations
const configuration 			= require(`${global.__base}/config`);
const { port, name, appDomain } = configuration;

// Next app
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = routes.getRequestHandler(app)

app.prepare().then(() => {
	const server = express();
	
	server.get('/redeem-detail/:id', (req, res) => {
		return app.render(req, res, '/redeem-detail', { id: req.params.id })
	})

	// server.get('/prize-detail/:id', (req, res) => {
	// 	return app.render(req, res, '/prize-detail', { id: req.params.id })
	// })
	
    // use Next router instead
	server.get('*', (req, res) => {
	    return handle(req, res)
	});
    
	server.listen(port, (err) => {
		if (err) throw err;
		console.log(`The ${name} listening at ${appDomain}`);
  	});
}).catch(ex => {
	console.error(ex.stack);
	process.exit(1);
})
