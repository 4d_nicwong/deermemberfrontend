const webpack = require('webpack');
const withCss = require('@zeit/next-css');
const withSass 	= require('@zeit/next-sass');
const path 		= require('path');
const config = require(`${global.__base}/config`);

const commonsChunkConfig = (config, test = /\.css$/) => {
    config.plugins = config.plugins.map(plugin => {
        if (
            plugin.constructor.name === 'CommonsChunkPlugin' &&
            // disable filenameTemplate checks here because they never match
            // (plugin.filenameTemplate === 'commons.js' ||
            //     plugin.filenameTemplate === 'main.js')
            // do check for minChunks though, because this has to (should?) exist
            plugin.minChunks != null
        ) {
            const defaultMinChunks = plugin.minChunks;
            plugin.minChunks = (module, count) => {
                if (module.resource && module.resource.match(test)) {
                    return true;
                }
                return defaultMinChunks(module, count);
            };
        }
        return plugin;
    });
    return config;
};

// console.log(path.resolve(__dirname, 'static/styles'));

module.exports = withSass({
	cssModules: true,
	cssLoaderOptions: {
		importLoaders: 1,
	    localIdentName: config.className,
	},
	webpack: (config, { isServer }) => {
		/*
		** Absolute Import Path
		** Terry Chan
		** 22/10/2018
		**/
	    config.resolve = {
		    alias: {
		    	Styles: path.resolve(__dirname, 'static/styles'),
				Components: path.resolve(__dirname, 'components/'),
				Pages: path.resolve(__dirname, 'pages/'),
				Client: path.resolve(__dirname, 'client/'),
				Reducers: path.resolve(__dirname, 'client/reducers/'),
				Helpers: path.resolve(__dirname, 'helpers/'),
		    },
		};
		return config;
	},
});
