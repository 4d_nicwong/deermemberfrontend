import React from 'react';
import Head from 'next/head';
import App, { Container } from 'next/app';
import { Provider } from "react-redux";
import withRedux from 'next-redux-wrapper';
import initStore from 'Client/store';
import AppContainer from 'Client/containers/AppContainer';
import 'Styles/global/main.scss';
import 'Styles/account/loginForm.scss';
import 'Styles/single/prizeList.scss';
import 'Styles/pages/prizeDetail.scss';
import 'Styles/pages/checkout.scss';
import 'Styles/pages/myAccount.scss';

class RootApp extends App {
  static async getInitialProps({ Component, ctx }) {

      const pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};

      return {
        pageProps,
      };

  }

  render () {
    const { Component, pageProps, store } = this.props;

    return (
      <Container>
          <Head>
            <title>Next Frontend</title>
            <link rel="stylesheet" href="/static/styles/global/reset.css"/>
            <link rel="stylesheet" href="/static/styles/global/bootstrap.min.css"/>
            <link rel="stylesheet" href="/static/styles/global/font-awesome.min.css"/>
            <link rel="stylesheet" href="/static/styles/global/phone-input.css"/> 
          </Head>
          <Provider store={store}>
              <AppContainer>
                <Component {...pageProps} />
              </AppContainer>
          </Provider>
      </Container>
    );
  }
}

export default withRedux(initStore)(RootApp);
