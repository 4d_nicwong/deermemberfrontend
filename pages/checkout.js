import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import PrivateRoute from 'Client/containers/common/PrivateRoute';
import Page from 'Components/common/Layout';
import Breadcrumb from 'Components/common/Breadcrumb';
import Link from 'Components/common/Link';
import NumberInput from 'Components/common/NumberInput';


import { fetchRedeemPrize } from 'Reducers/checkout';
import Styles from 'Styles/pages/checkout.scss';

class Checkout extends Component {

    constructor(props) {
        super(props);
        const { quantity, itemCost } = props;
        this.state = {
            quantity,
            itemCost,
            totalPoint: itemCost * quantity,
            termsError: false,
        }
        this.setTotalPoint = this.setTotalPoint.bind(this);
        this.handleCheckout = this.handleCheckout.bind(this);
    }

    handleCheckout() {
        const chkTerms = this.refs.terms;
        if (!chkTerms.checked) {
            this.setState({ termsError: !chkTerms.checked });
            return false;
        }

        const { fetchRedeemPrize, prizeDetail } = this.props;
        const { quantity } = this.state;
        fetchRedeemPrize({
            prizeID: prizeDetail._id,
            quantity,
        });
    }
    
    setTotalPoint(number) {
        const { itemCost } = this.state;
        this.setState({
            quantity: number,
            totalPoint: number * itemCost,
        })
    }

    renderCheckoutTableHeader() {
        return (
            <thead>
                <tr>
                    <td>Prize item(s)</td>
                    <td>Quantity</td>
                    <td>Point required</td>
                </tr>
            </thead>
        )
    }

    renderCheckoutTableBody() {
        const { quantity, prizeDetail } = this.props;
        const { itemCost } = this.state;
        return (
            <tbody>
                <tr>
                    <td>
                        <div className='row'>
                            <span className='col-3'><img src={prizeDetail.picture.url}/></span>
                            <span className='col-9'><h5>{prizeDetail.name}</h5></span>
                        </div>
                    </td>
                    <td><NumberInput value={quantity} getNumber={this.setTotalPoint}/></td>
                    <td><i className="fas fa-copyright"/>{itemCost}</td>
                </tr>
            </tbody>
        )
    }

    renderCheckoutTableFooter() {
        const { totalPoint } = this.state;
        return (
            <tfoot>
                <tr>
                    <td></td>
                    <td>Total:</td>
                    <td><span><i className="fas fa-copyright"/>{totalPoint}</span></td>
                </tr>
            </tfoot>
        )
    }

    renderCheckout() {
        const { termsError } = this.state;
        const errorClass = termsError ? 'termsError' : null;
        return (
            <div className='row checkoutBox'>
                <div className={['col-10', errorClass].join(' ')}>
                    <div><input type='checkbox' name='terms' id='terms' ref='terms'></input></div>
                    <label htmlFor='terms'>I acknowledge that i have read and understood the above Important Notes and agreed to be bound by the Terms and Conditions.</label>
                    <span>＊ Required</span>
                </div>
                <div className='col-2'><a className='checkoutBtn' onClick={this.handleCheckout}>Checkout</a></div>
            </div>
        )
    }

    renderCheckoutSection() {
        return (
            <React.Fragment>
                {/* <Breadcrumb links={[
                    { text: 'Home', href: '/' },
                    { text: 'Our Recommendations', href: '/' },
                    { text: 'Smartech “Mini Eco Fresh” Intelligent Dehumidifier (SD-1800) (1pc)', href: '/prize-detail' },
                    { text: 'Checkout' }
                ]}/> */}
                <table>
                    {this.renderCheckoutTableHeader()}
                    {this.renderCheckoutTableBody()}
                    {this.renderCheckoutTableFooter()}
                </table>
                {this.renderCheckout()}
            </React.Fragment>
        )
    }

    renderRedeemSuccess() {
        return (
            <div className='checkoutSuccess'>
                <h4>Redeem success!</h4>
            </div>
        )
    }

    render() {
        const { prizeDetail, redeemSuccess, router } = this.props;
        if (!prizeDetail) {
            router.push('/');
            return false;
        }
        return (
            <Page>
                <div className='checkout'>
                    {redeemSuccess ? this.renderRedeemSuccess() : this.renderCheckoutSection()}
                </div>
            </Page>
        )
    }
}

const mapStateToProps = state => {
    const prizeDetail = state.get('prize').toJS().prizeDetail;
    const checkout = state.get('checkout').toJS();
    return {
        quantity: checkout.quantity,
        itemCost: prizeDetail ? prizeDetail.pointNeeded : 0,
        prizeDetail,
        redeemSuccess: checkout.redeemPrizeSuccess,
    }
}

const mapDispatchToProps = {
    fetchRedeemPrize
};

const connectCheckout = connect(mapStateToProps, mapDispatchToProps)(Checkout);

export default withRouter(connectCheckout);