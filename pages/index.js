import React, { Component } from "react";
import { GridLoader } from 'react-spinners';
import Head from 'next/head';
import Page from 'Components/common/Layout';
// import Breadcrumb from 'Components/common/Breadcrumb';
import PrizeList from 'Components/PrizeList';

const pageTitle = 'Deer Member';

class Index extends Component {

    static async getInitialProps({ req }) {
		if (req && req.params && req.params.id) {
		    const { params: { id } } = req;
		    return { id };
		}
		return {};
    }

    renderAboutUs() {
        return (
            <div className="index-top mb-5">
                <h1>About us</h1>
                <p>Our design team can always adapt to various kinds of design styles such as being fresh, lovely, minimalist, gorgeous and so on. From the interaction design to the perfect coordination between the front-end interface and back-end system, our experienced developers handle challenges with ease as well. Want to obtain a satisfied app? All you need is a considerate team.</p>
            </div>
        )
    }
    
    render() {
        return (
            <Page>
                <GridLoader loading={true}/>
                <Head>
                    <title>{pageTitle}</title>
                </Head>
                    {/* <Breadcrumb links={[
                        { text: 'Home', href: '/' },
                        { text: pageTitle }
                    ]}/> */}
                    {/* <h1 className={Styles.pageTitle}>{pageTitle}</h1>
                    <div className={Styles.banner}><img src='static/images/homepage_banner.jpg'></img></div> */}
                {this.renderAboutUs()}
                <PrizeList/>
            </Page>
        )
    }
}

export default Index;
