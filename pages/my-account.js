import Head from 'next/head';
import React, { Component } from "react";
import { withRouter } from 'next/router';
import { connect } from "react-redux";
import Page from 'Components/common/Layout';
import Link from 'Components/common/Link';
import Breadcrumb from 'Components/common/Breadcrumb';

import { fetchMemberStatus } from 'Reducers/member/actions/getStatus'
import Styles from 'Styles/pages/prizeDetail.scss';

class MyAccount extends Component {
	static async getInitialProps({ req }) {
		if (req && req.params && req.params.id) {
		    const { params: { id } } = req;
		    return { id };
		}
		return {};
	}

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.fetchMemberStatus();
    }

    renderAccountInfo() {
        const { accountInfo } = this.props;
        return (
            <div className='accountInfo'>
                <h3>My Account</h3>
                <div className='row'><span className='col-6'>Member ID</span><span className='col-6'>{accountInfo._id}</span></div>
                <div className='row'><span className='col-6'>Title:</span><span className='col-6'>{accountInfo.title}</span></div>
                <div className='row'><span className='col-6'>Last Name:</span><span className='col-6'>{accountInfo.fullname.last}</span></div>
                <div className='row'><span className='col-6'>First Name:</span><span className='col-6'>{accountInfo.fullname.first}</span></div>
                <div className='row'><span className='col-6'>Phone:</span><span className='col-6'>{accountInfo.phone}</span></div>
                <div className='row'><span className='col-6'>Point:</span><span className='col-6'><i className="fas fa-copyright"/>{accountInfo.point}</span></div>
            </div>
        )
    }

    renderRedeemHistory() {
        const { accountInfo } = this.props;
        const historyList = accountInfo.histories.map((item, key) => {
            return (
                <div className='row' key={key}>
                    <span className='col-3'>{item._id}</span>
                    <span className='col-2'>{item.redeemAt}</span>
                    <span className='col-3'>{item.name}</span>
                    <span className='col-2'><i className="fas fa-copyright"/>{item.totalAmount}</span>
                    <span className='col-2'><Link href='/redeem-detail' className='redeemDetail' params={{id: item._id}}>Detail</Link></span>
                </div>
            )
        })
        return (
            <div className='history'>
                <h3>Redeem History</h3>
                <div className='row header'>
                    <span className='col-3'>Redeem ID</span>
                    <span className='col-2'>Date & Time</span>
                    <span className='col-3'>Prize</span>
                    <span className='col-2'>Point</span>
                    <span className='col-2'>More Info</span>
                </div>
                {historyList}
            </div>
        )
    }

    render() {
        const { isSuccess } = this.props
        return (
            <Page>
                <div className='myAccount'>
                    <Head>
                        <title>My account</title>
                    </Head>
                    {/* <Breadcrumb links={[
                        { text: 'Home', href: '/' },
                        { text: 'My account' },
                    ]}/> */}
                    {isSuccess && this.renderAccountInfo()}
                    {isSuccess && this.renderRedeemHistory()}
                </div>
            </Page>
        )
    }
}

const mapStateToProps = state => ({
    accountInfo: state.get('member').toJS().status.info,
    isSuccess: state.get('member').toJS().status.isSuccess,
})

const mapDispatchToProps = {
    fetchMemberStatus
};

const connectMyAccount = connect(mapStateToProps, mapDispatchToProps)(MyAccount);

export default withRouter(connectMyAccount);