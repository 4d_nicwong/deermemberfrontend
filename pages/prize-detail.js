import Head from 'next/head';
import React, { Component } from "react";
import { withRouter } from 'next/router';
import { connect } from "react-redux";
import Page from 'Components/common/Layout';
import Link from 'Components/common/Link';
import Breadcrumb from 'Components/common/Breadcrumb';
import NumberInput from 'Components/common/NumberInput';

import { fetchPrizeDetail } from 'Reducers/prize';
import { setPrizeQuantity } from 'Reducers/checkout';
import Styles from 'Styles/pages/prizeDetail.scss';

class PrizeDetail extends Component {
	static async getInitialProps({ req }) {
		if (req && req.params && req.params.id) {
		    const { params: { id } } = req;
		    return { id };
		}
		return {};
	}

    constructor(props) {
        super(props);
        this.state = {
            quantity: 1,
            activeTab: 'detail',
        };
        this.handleCheckout = this.handleCheckout.bind(this);
        this.getQuantity = this.getQuantity.bind(this);
        this.changeTab = this.changeTab.bind(this);
    }

    componentWillMount() {
        this.props.fetchPrizeDetail(this.props.id);
    }

    // componentWillReceiveProps(nextProps, nextState) {
    //     const { prizeDetailSuccess, prizeDetail } = nextProps;
    //     if (prizeDetailSuccess) {
    //         this.setState({
    //             prizeDetail: prizeDetail
    //         })
    //     }
    // }

    handleCheckout() {
        const { setPrizeQuantity, router, id, isLoggedIn  } = this.props;
        setPrizeQuantity(this.state.quantity);
        if(isLoggedIn) {
            router.push('/checkout');
        }
        else{
            router.push('/signin');
        }
    }

    getQuantity(number) {
        this.setState({ quantity: number })
    }

    changeTab(tab) {
        this.setState({ activeTab: tab })
    }

    renderPrizeInfo() {
        const { prizeDetail } = this.props;
        return (
            <div className='row prizeInfo'>
                <div className='col-5 imageCol'><img src={prizeDetail.picture.url}/></div>
                <div className='col-7 infoCol'>
                    <div cla><h4>{prizeDetail.name}</h4></div>
                    <div className='prizeID'>Prize ID:<span>{prizeDetail._id}</span></div>
                    <div className='point'><i className="fas fa-copyright"/><span>{prizeDetail.pointNeeded}</span></div>
                    <div className='field'><span>Delivery:</span><span>Courier Delivery</span></div>
                    <div className='quantity field'>
                        <span>Quantity:</span>
                        <span><NumberInput getNumber={this.getQuantity}/></span>
                    </div>
                    <div className='checkout'><a className='checkoutBtn' onClick={this.handleCheckout}>Checkout</a></div>
                    <div className='share'>
                        <i className="fab fa-facebook"/>
                        <i className="far fa-envelope"/>
                        <i className="far fa-copy"/>
                    </div>
                </div>
            </div>
        )
    }

    renderDescriptionTab() {
        const { prizeDetail } = this.props;
        const { activeTab } = this.state;
        let content = 'No content';
        switch (activeTab) {
            case 'detail': { content = prizeDetail.content; break; }
            case 'redeem': { content = prizeDetail.redeemDescription; break; }
            case 'terms': { content = prizeDetail.tcDescription; break; }
        }
        const activeClass = tab => (activeTab === tab ? 'activeTab' : null)
        return (
            <div className='descriptionTab'>
                <ul>
                    <li className={activeClass('detail')} onClick={() => {this.changeTab('detail')}}>Detail</li>
                    <li className={activeClass('redeem')} onClick={() => {this.changeTab('redeem')}}>How to Redeem</li>
                    <li className={activeClass('terms')} onClick={() => {this.changeTab('terms')}}>Terms & Conditions</li>
                </ul>
                <div dangerouslySetInnerHTML={{__html: content}}></div>
            </div>
        )
    }

    render() {
        const { prizeDetailSuccess, prizeDetail, router } = this.props;
        // const pageTitle = prizeDetail ? prizeDetail.name : '';
        // if (!prizeDetail) {
        //     router.push('/');
        //     return false;
        // }
        return (
            <Page>
                <div className='prizeDetail'>
                    <Head>
                        <title>Prize Detail</title>
                    </Head>
                    {/* <Breadcrumb links={[
                        { text: 'Home', href: '/' },
                        { text: 'Our Recommendations', href: '/' },
                        { text: pageTitle }
                    ]}/> */}
                    {prizeDetailSuccess && this.renderPrizeInfo()}
                    {prizeDetailSuccess && this.renderDescriptionTab()}
                </div>
            </Page>
        )
    }
}

const mapStateToProps = state => ({
    prizeDetailSuccess: state.get('prize').toJS().prizeDetailSuccess,
    prizeDetail:  state.get('prize').toJS().prizeDetail,
    isLoggedIn: state.get('member').toJS().auth.isLoggedIn,
})

const mapDispatchToProps = {
    fetchPrizeDetail,
    setPrizeQuantity,
};

const connectPrizeDetail = connect(mapStateToProps, mapDispatchToProps)(PrizeDetail);

export default withRouter(connectPrizeDetail);