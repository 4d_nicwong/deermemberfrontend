import Head from 'next/head';
import React, { Component } from "react";
import { connect } from "react-redux";
import Page from 'Components/common/Layout';
import Breadcrumb from 'Components/common/Breadcrumb';
import PrizeList from 'Components/PrizeList';
import Styles from 'Styles/pages/recommendation.scss';

const pageTitle = 'Deer Member';

class Recommendation extends Component {
	static async getInitialProps({ req }) {
		if (req && req.params && req.params.id) {
		    const { params: { id } } = req;
		    return { id };
		}
		return {};
	}

    render() {
        return (
        	<Page>
                <div className={Styles.recommendation}>
                    <Head>
                        <title>{pageTitle}</title>
                    </Head>
                    {/* <Breadcrumb links={[
                        { text: 'Home', href: '/' },
                        { text: pageTitle }
                    ]}/> */}
                    {/* <h1 className={Styles.pageTitle}>{pageTitle}</h1>
                    <div className={Styles.banner}><img src='static/images/homepage_banner.jpg'></img></div> */}
                    <PrizeList/>
                </div>
            </Page>
        )
    }
}

const mapStateToProps = state => {
    return {
    };
};

export default connect(mapStateToProps, {})(Recommendation);
