import Head from 'next/head';
import React, { Component } from "react";
// import { withRouter } from 'next/router';
import { connect } from "react-redux";
import Page from 'Components/common/Layout';
import Link from 'Components/common/Link';
import Breadcrumb from 'Components/common/Breadcrumb';

import { fetchMemberStatus } from 'Reducers/member/actions/getStatus'
import Styles from 'Styles/pages/prizeDetail.scss';

class MyAccount extends Component {
	static async getInitialProps({ req }) {
		if (req && req.params && req.params.id) {
		    const { params: { id } } = req;
		    return { id };
		}
		return {};
	}

    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.props.fetchMemberStatus();
    }

    renderRedeemDetail() {
        const { histories, id } = this.props;
        // const item = histories.find(item => item._id === id);
        const item = histories[0];
        return (
            <div className='redeem-info'>
                <h3>Redeem Detail</h3>
                <div className='row'><span className='col-6'>Redeem ID</span><span className='col-6'>{item._id}</span></div>
                <div className='row'><span className='col-6'>Prize:</span><span className='col-6'>{item.name}</span></div>
                <div className='row'><span className='col-6'>Prize Point:</span><span className='col-6'><i className="fas fa-copyright"/>{item.point}</span></div>
                <div className='row'><span className='col-6'>Quantity:</span><span className='col-6'>{item.quantity}</span></div>
                <div className='row'><span className='col-6'>Date & Time:</span><span className='col-6'>{item.redeemAt}</span></div>
            </div>
        )
    }

    render() {
        // const { isSuccess } = this.props
        // console.log('@@@@@@@@@@@@@@ props', this.props);
        return (
            <Page>
                <div className='redeem-detail'>
                    <Head>
                        <title>My account</title>
                    </Head>
                    {this.renderRedeemDetail()}
                    {/* id: {this.props.id} */}
                </div>
            </Page>
        )
    }
}

const mapStateToProps = state => ({
    histories: state.get('member').toJS().status.info.histories,
    isSuccess: state.get('member').toJS().status.isSuccess,
})

const mapDispatchToProps = {
    fetchMemberStatus
};

const connectMyAccount = connect(mapStateToProps, mapDispatchToProps)(MyAccount);

export default connectMyAccount;