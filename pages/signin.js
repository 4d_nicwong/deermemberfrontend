import Head from 'next/head';
import React, { Component } from "react";
import { connect } from "react-redux";
import Router, { withRouter } from 'next/router';
import Page from 'Components/common/Layout';
import LoginForm from 'Client/containers/account/LoginForm';
import Link from 'Components/common/Link';

const pageTitle = 'Login';

class Signin extends Component {
	static async getInitialProps({ req }) {
		if (req && req.params && req.params.id) {
		    const { params: { id } } = req;
		    return { id };
		}
		return {};
    }

    constructor(props){
        super(props);

        this.state = {
            redirect: false
        }
    }

    componentWillReceiveProps(nextProps){
        const { isSuccess } = nextProps;
        if(isSuccess) {
            Router.push('/checkout');
        }
    }
    
    render() {
        // console.log('>>1>>>', this.props);
        return (
            <Page>
                <Head>
                    <title>{pageTitle}</title>
                </Head>
                <h1 className="text-center mb-5">{pageTitle}</h1>
                <LoginForm/>
                <div className="text-center mt-4">
                    <Link href="/register" className="withLine">Forgot password</Link>
                </div>
            </Page>
        )
    }
}

const mapStateToProps = state => {
    const memberJS = state.get('member').toJS();
    const { auth: { isSuccess } } = memberJS;
    return ({
        isSuccess
    });
};

export default withRouter(connect(mapStateToProps, {})(Signin));